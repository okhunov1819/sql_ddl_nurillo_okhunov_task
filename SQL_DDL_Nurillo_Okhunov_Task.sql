


---- PLEASE, IN ORDER TO INITIATE CODE, UNCOMMENT EACH STEP ONE AT A TIME !!!!!!!!!!!



---- Step 1: Create the physical database
--CREATE DATABASE database_nurillo;



---- Step 2: Connect to the newly created database (by selecting DBMS tool).



---- Step 3: Create the schema
--CREATE SCHEMA nuri_database_schema;



---- Step 4: Create tables based on the 3NF model
--CREATE TABLE nuri_database_schema.epam_students (
--    student_id SERIAL PRIMARY KEY,
--    student_name VARCHAR(100) NOT NULL,
--    student_email VARCHAR(100) NOT NULL,
--    student_phone VARCHAR(20),
--    student_address TEXT,
--    record_ts TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
--);

--CREATE TABLE nuri_database_schema.epam_trainings (
--    training_id SERIAL PRIMARY KEY,
--    student_id INT NOT NULL,
--    training_date DATE NOT NULL DEFAULT CURRENT_DATE,
--    unit_price DECIMAL(10, 2) NOT NULL,
--    quantity INT NOT NULL, --this column may not make sense for this table but I needed for "GENERATED ALWAYS AS"
--    training_total DECIMAL(10, 2) GENERATED ALWAYS AS (unit_price * quantity) STORED,
--    FOREIGN KEY (student_id) REFERENCES dos_database_schema.epam_students(student_id)
--);



---- Step 5: Apply check constraints
--ALTER TABLE dos_database_schema.epam_trainings
--ADD CONSTRAINT trainings_check_training_date CHECK (training_date > '2000-01-01');
--
--ALTER TABLE dos_database_schema.epam_trainings
--ADD CONSTRAINT trainings_check_positive_value CHECK (training_total >= 0);
--
--ALTER TABLE dos_database_schema.epam_students
--ADD CONSTRAINT students_check_gender CHECK (student_gender IN ('Male', 'Female'));
--
--ALTER TABLE dos_database_schema.epam_students
--ADD CONSTRAINT students_check_unique_email UNIQUE (student_email);
--
--ALTER TABLE dos_database_schema.epam_students
--ALTER COLUMN student_name SET NOT NULL;



---- Step 6: Populate tables with sample data
--INSERT INTO dos_database_schema.epam_students (student_name, student_email, student_phone, student_address)
--VALUES ('John Doe', 'john@example.com', '1234567890', '123 Main St');
--
--INSERT INTO dos_database_schema.epam_students (student_name, student_email)
--VALUES ('Jane Smith', 'jane@example.com');
--
--INSERT INTO dos_database_schema.epam_trainings (student_id, unit_price, quantity)
--VALUES (1, 10.50, 3);
--
--INSERT INTO dos_database_schema.epam_trainings (student_id, unit_price, quantity)
--VALUES (2, 12.25, 5);
--
-- Verify the changes
--SELECT * FROM dos_database_schema.epam_students;
--SELECT * FROM dos_database_schema.epam_trainings;